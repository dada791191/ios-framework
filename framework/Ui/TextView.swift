//
//  TextView.swift
//  p2p_ios
//
//  Created by 顏巨鴻 on 2018/12/27.
//  Copyright © 2018 顏巨鴻. All rights reserved.
//

import UIKit

class TextView: UITextView{
    private var originalTextColour: UIColor = UIColor.black
    private var placeholderTextColour: UIColor = UIColor(red: 0, green: 0, blue: 0.098, alpha: 0.22)
    
    var placeholder:String?{
        didSet{
            if let placeholder = placeholder{
                text = placeholder
            }
        }
    }
    
    override internal var text: String? {
        didSet{
            textColor = originalTextColour
            if text == placeholder{
                textColor = placeholderTextColour
            }
        }
    }
    
    override internal var textColor: UIColor?{
        didSet{
            if let textColor = textColor, textColor != placeholderTextColour{
                originalTextColour = textColor
                if text == placeholder{
                    self.textColor = placeholderTextColour
                }
            }
        }
    }
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.textContainer.lineFragmentPadding = 0
        self.textContainerInset = UIEdgeInsets.zero
        
        // Listen for text view did begin editing
        NotificationCenter.default.addObserver(self, selector: #selector(removePlaceholder), name: UITextView.textDidBeginEditingNotification, object: nil)
        // Listen for text view did end editing
        NotificationCenter.default.addObserver(self, selector: #selector(addPlaceholder), name: UITextView.textDidEndEditingNotification, object: nil)
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let barButtonItem = UIBarButtonItem.init(title: "Done", style: .plain, target: self, action: #selector(finishEdit))
        let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 44))
        toolBar.barStyle = .default
        toolBar.isUserInteractionEnabled = true
        toolBar.items = [spaceButton, barButtonItem]
        inputAccessoryView = toolBar

    }
    
    @objc private func finishEdit(){
        resignFirstResponder()
    }
    
    @objc private func removePlaceholder(){
        if text == placeholder{
            text = ""
        }
    }
    
    @objc private func addPlaceholder(){
        if text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == "" {
            text = placeholder
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
