//
//  Notification.swift
//  p2p_ios
//
//  Created by dada on 2018/11/21.
//  Copyright © 2018 dada. All rights reserved.
//
import UserNotifications

extension Notification.Name {
    //
    public static let TabbarDismiss = Notification.Name(rawValue: "TabbarDismiss")
    public static let UserInfoChangeLocation = Notification.Name(rawValue: "UserInfoChangeLocation")
}
