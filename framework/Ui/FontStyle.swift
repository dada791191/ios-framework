//
//  UIFont.swift
//  p2p_ios
//
//  Created by 顏巨鴻 on 2018/11/16.
//  Copyright © 2018 顏巨鴻. All rights reserved.
//

import Foundation
import UIKit

final class FontStyle{
    static let sharedInstance:FontStyle = FontStyle()
    
    private let fontName = "Helvetica"
    private let fontNameBlod = "Helvetica-Bold"
    
    // MARK: - 自訂方法
    func getFontSize(size: CGFloat)->UIFont{
        return UIFont(name: fontName, size: size)!
    }
    
    func getFontSizeBold(size: CGFloat)->UIFont{
        return UIFont(name: fontNameBlod, size: size)!
    }
}
