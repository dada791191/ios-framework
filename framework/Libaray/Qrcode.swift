//
//  Qrcode.swift
//  like_life
//
//  Created by 顏巨鴻 on 2019/11/13.
//  Copyright © 2019 顏巨鴻. All rights reserved.
//

import Foundation
import UIKit

class Qrcode {
    func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)

        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 10, y: 10)

            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }

        return nil
    }
}
