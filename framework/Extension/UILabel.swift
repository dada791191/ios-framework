//
//  UILabel.swift
//  like_life
//
//  Created by admin on 2019/11/6.
//  Copyright © 2019 顏巨鴻. All rights reserved.
//

import Foundation
import UIKit
extension UILabel {
    // 在label + ICON
    func addImageWith(name: String, behindText: Bool, w:Int, h:Int, merge:Int, space:Float, color: UIColor = UIColor(rgb: 0x84827f), isParaph: Bool = false, font: UIFont = FontStyle.sharedInstance.getFontSize(size: 15)) {
        let paraph = NSMutableParagraphStyle()
        paraph.lineSpacing = 12
        
        let attachment = NSTextAttachment()
        attachment.image = UIImage(named: name)
        attachment.bounds = CGRect(x: 0, y: merge, width: w, height: h)

        let attachmentString = NSAttributedString(attachment: attachment)

        guard let txt = self.text else {
            return
        }

        if behindText {
            let strLabelText = NSMutableAttributedString(string: txt)
            strLabelText.append(attachmentString)
            strLabelText.addAttribute(.kern, value: space, range: NSRange(location: 0, length: strLabelText.length))
            strLabelText.addAttribute(.font, value: font, range: NSRange(location: 0, length: strLabelText.length))
            strLabelText.addAttribute(.foregroundColor, value: color, range: NSRange(location: 0, length: strLabelText.length))
            if isParaph == true{
                strLabelText.addAttribute(.paragraphStyle, value: paraph, range: NSRange(location: 0, length: strLabelText.length))
            }
            
            self.attributedText = strLabelText
            
        } else {
            let strLabelText = NSAttributedString(string: txt)
            let mutableAttachmentString = NSMutableAttributedString(attributedString: attachmentString)
            
            mutableAttachmentString.append(strLabelText)
            mutableAttachmentString.addAttribute(.kern, value: space, range: NSRange(location: 0, length: mutableAttachmentString.length))
            mutableAttachmentString.addAttribute(.font, value: font, range: NSRange(location: 0, length: strLabelText.length))
            mutableAttachmentString.addAttribute(.foregroundColor, value: color, range: NSRange(location: 0, length: strLabelText.length))
            if isParaph == true{
                mutableAttachmentString.addAttribute(.paragraphStyle, value: paraph, range: NSRange(location: 0, length: strLabelText.length))
            }
            self.attributedText = mutableAttachmentString
        }
    }

    func removeImage() {
        let text = self.text
        self.attributedText = nil
        self.text = text
    }
    
    func setLetterSpacing(text: String, withKerning kerning: CGFloat) {
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.kern, value: kerning, range: NSMakeRange(0, text.count))

        attributedText = attributedString
    }
    func addTrailing(with trailingText: String, moreText: String, moreTextFont: UIFont, moreTextColor: UIColor) {
        let readMoreText: String = trailingText + moreText
        
        let lengthForVisibleString: Int = self.vissibleTextLength
        
        let mutableString: String = self.text!
        
        let trimmedString: String? = (mutableString as NSString).replacingCharacters(in: NSRange(location: lengthForVisibleString, length: ((self.text?.count)! - lengthForVisibleString)), with: "")
        
        let readMoreLength: Int = (readMoreText.count)
        
        let trimmedForReadMore: String = (trimmedString! as NSString).replacingCharacters(in: NSRange(location: ((trimmedString?.count ?? 0) - readMoreLength), length: readMoreLength), with: "") + trailingText
       
        let answerAttributed = NSMutableAttributedString(string: trimmedForReadMore, attributes: [NSAttributedString.Key.font: self.font!])
        
        let readMoreAttributed = NSMutableAttributedString(string: moreText, attributes: [NSAttributedString.Key.font: moreTextFont, NSAttributedString.Key.foregroundColor: moreTextColor])
        
        answerAttributed.append(readMoreAttributed)
        self.attributedText = answerAttributed
    }
    
    var maxNumberOfLines: Int {
        let maxSize = CGSize(width: frame.size.width, height: CGFloat(MAXFLOAT))
        let string = self.text!.replacingOccurrences(of: "\n", with: "  ")
        let text = string as NSString
        let textHeight = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [.font: font!], context: nil).height
        let lineHeight = font!.lineHeight


        return Int(ceil(textHeight / lineHeight))
    }
    
    var vissibleTextLength: Int {
        let font: UIFont = self.font
        let mode: NSLineBreakMode = self.lineBreakMode
        let labelWidth: CGFloat = self.frame.size.width
        let labelHeight: CGFloat = self.frame.size.height
        let sizeConstraint = CGSize(width: labelWidth, height: CGFloat.greatestFiniteMagnitude)

        let attributes: [AnyHashable: Any] = [NSAttributedString.Key.font: font]
        let attributedText = NSAttributedString(string: self.text!, attributes: attributes as? [NSAttributedString.Key : Any])
        let boundingRect: CGRect = attributedText.boundingRect(with: sizeConstraint, options: .usesLineFragmentOrigin, context: nil)
        
        if boundingRect.size.height > labelHeight {
            var index: Int = 0
            var prev: Int = 0
            let characterSet = CharacterSet.whitespacesAndNewlines
            
            repeat {
                prev = index
                if mode == NSLineBreakMode.byCharWrapping {
                    index += 1
                } else {
                    index = (self.text! as NSString).rangeOfCharacter(from: characterSet, options: [], range: NSRange(location: index + 1, length: self.text!.count - index - 1)).location
                }
            } while index != NSNotFound && index < self.text!.count && (self.text! as NSString).substring(to: index).boundingRect(with: sizeConstraint, options: .usesLineFragmentOrigin, attributes: attributes as? [NSAttributedString.Key : Any], context: nil).size.height <= labelHeight
            
            return prev
        }
        return self.text!.count
    }
    
    func setLineSpacing(lineSpacing: CGFloat = 0.0, lineHeightMultiple: CGFloat = 0.0) {

        guard let labelText = self.text else { return }

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = lineSpacing
        paragraphStyle.lineHeightMultiple = lineHeightMultiple

        let attributedString:NSMutableAttributedString
        if let labelattributedText = self.attributedText {
            attributedString = NSMutableAttributedString(attributedString: labelattributedText)
        } else {
            attributedString = NSMutableAttributedString(string: labelText)
        }

        // Line spacing attribute
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))

        self.attributedText = attributedString
    }
}
