//
//  LoadingView.swift
//  like_life
//
//  Created by 顏巨鴻 on 2019/12/4.
//  Copyright © 2019 顏巨鴻. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class LoadingView: UIView {
    
    private let kScreenW: CGFloat = UIScreen.main.bounds.width
    private let kScreenH: CGFloat = UIScreen.main.bounds.height
    private let round1Color = UIColor(red: 81, green: 188, blue: 62)
    private let round2Color = UIColor(red: 246, green: 201, blue: 51)
    private let round3Color = UIColor(red: 225, green: 41, blue: 34)
    private let animTime = 1.5
    private let animRepeatTime:Float = 50
    private let kRoundW: CGFloat = 10
    private let subView: UIView = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override var layer: CAShapeLayer {
        get {
            return super.layer as! CAShapeLayer
        }
    }

    override class var layerClass: AnyClass {
        return CAShapeLayer.self
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        layer.fillColor = nil
        layer.strokeColor = UIColor.black.cgColor
        layer.lineWidth = 3
        setPath()
    }

    override func didMoveToWindow() {
        animate()
    }

    private func setPath() {
        layer.path = UIBezierPath(ovalIn: bounds.insetBy(dx: layer.lineWidth / 2, dy: layer.lineWidth / 2)).cgPath
    }

    struct Pose {
        let secondsSincePriorPose: CFTimeInterval
        let start: CGFloat
        let length: CGFloat
        init(_ secondsSincePriorPose: CFTimeInterval, _ start: CGFloat, _ length: CGFloat) {
            self.secondsSincePriorPose = secondsSincePriorPose
            self.start = start
            self.length = length
        }
    }

    class var poses: [Pose] {
        get {
            return [
                Pose(0.0, 0.000, 0.7),
                Pose(0.6, 0.500, 0.5),
                Pose(0.6, 1.000, 0.3),
                Pose(0.6, 1.500, 0.1),
                Pose(0.2, 1.875, 0.1),
                Pose(0.2, 2.250, 0.3),
                Pose(0.2, 2.625, 0.5),
                Pose(0.2, 3.000, 0.7),
            ]
        }
    }

    func animate() {
        var time: CFTimeInterval = 0
        var times = [CFTimeInterval]()
        var start: CGFloat = 0
        var rotations = [CGFloat]()
        var strokeEnds = [CGFloat]()

        let poses = type(of: self).poses
        let totalSeconds = poses.reduce(0) { $0 + $1.secondsSincePriorPose }

        for pose in poses {
            time += pose.secondsSincePriorPose
            times.append(time / totalSeconds)
            start = pose.start
            rotations.append(start * 2 * .pi)
            strokeEnds.append(pose.length)
        }

        times.append(times.last!)
        rotations.append(rotations[0])
        strokeEnds.append(strokeEnds[0])

        animateKeyPath(keyPath: "strokeEnd", duration: totalSeconds, times: times, values: strokeEnds)
        animateKeyPath(keyPath: "transform.rotation", duration: totalSeconds, times: times, values: rotations)

        animateStrokeHueWithDuration(duration: totalSeconds * 5)
    }

    func animateKeyPath(keyPath: String, duration: CFTimeInterval, times: [CFTimeInterval], values: [CGFloat]) {
        let animation = CAKeyframeAnimation(keyPath: keyPath)
        animation.keyTimes = times as [NSNumber]?
        animation.values = values
        animation.calculationMode = .linear
        animation.duration = duration
        animation.repeatCount = Float.infinity
        layer.add(animation, forKey: animation.keyPath)
    }

    func animateStrokeHueWithDuration(duration: CFTimeInterval) {
        let count = 36
        let animation = CAKeyframeAnimation(keyPath: "strokeColor")
        animation.keyTimes = (0 ... count).map { NSNumber(value: CFTimeInterval($0) / CFTimeInterval(count)) }
        animation.values = (0 ... count).map {
           UIColor(hue: CGFloat($0) / CGFloat(count), saturation: 1, brightness: 1, alpha: 1).cgColor
        }
        animation.duration = duration
        animation.calculationMode = .linear
        animation.repeatCount = Float.infinity
        layer.add(animation, forKey: animation.keyPath)
    }
    
    func show(_ vc: UIViewController) {
        if (vc.navigationController != nil) {
            if vc.navigationController!.isNavigationBarHidden {
                self.subView.frame = CGRect(x: 0, y: 0 - (vc.navigationController?.navigationBar.frame.height)!, width: vc.view.bounds.width, height: vc.view.bounds.height + (vc.navigationController?.navigationBar.frame.height)!)
            } else {
                self.subView.frame = vc.view.bounds
            }
        } else {
            self.subView.frame = vc.view.bounds
        }
        
        self.subView.backgroundColor = .black
        self.subView.alpha = 0.3
        self.subView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        let currentWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        currentWindow!.addSubview(self.subView)
        
        let loadingViewWidth = vc.view.bounds.size.width * 0.15
        let loadingViewHeight = loadingViewWidth
        
        self.frame = CGRect(x: vc.view.bounds.size.width / 2 - loadingViewWidth / 2, y: vc.view.bounds.size.height / 2 - loadingViewHeight / 2, width: loadingViewWidth, height: loadingViewHeight)
        self.subView.addSubview(self)
    }
    
    func remove() {
        self.subView.removeFromSuperview()
    }
}
