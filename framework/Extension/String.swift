//
//  String.swift
//  like_life
//
//  Created by dada on 2019/10/16.
//  Copyright © 2019 dada. All rights reserved.
//

import Foundation
import CryptoSwift
import CommonCrypto
import UIKit

extension String {
    
    private enum CryptoError: LocalizedError {
        case runtimeError(String)
    }
    
    private func createAES(textBytes: [UInt8], iv: [UInt8]) throws -> AES! {
        do {
            if let keyData = "2oinakpeiy87ggla".data(using: .utf8) {
                return try AES(key: keyData.sha256().bytes, blockMode:  CFB(iv: iv), padding: .noPadding)
            } else {
                throw CryptoError.runtimeError("createAES keyData error")
            }
        } catch {
            throw CryptoError.runtimeError("createAES AES error")
        }
    }
    // 加密
    func encrypto() -> String {
        if self == "" {
            return ""
        }
        var result = ""
        do {
            let textBytes = [UInt8](self.utf8)
            var ivData = Data(count: 16)
            let status = ivData.withUnsafeMutableBytes {
                 SecRandomCopyBytes(kSecRandomDefault, 16, $0.baseAddress!)
            }
            if status == errSecSuccess {
                let iv = ivData.bytes
                let aes = try createAES(textBytes: textBytes, iv: iv)!
                let encrypted = try aes.encrypt(textBytes)
                let data = iv + encrypted
                result = data.toBase64()!
            } else {
               print("Problem generating random bytes")
            }
        } catch CryptoError.runtimeError(let errorMessage){
            print(errorMessage)
        } catch {
            print("encrypto error")
        }
        return result
    }
    // 解密
    func decrypto() -> String {
        if self == "" {
           return ""
        }
        
        var result = ""
        do {
            if let textBase64 = Data(base64Encoded: self) {
                let textBytes = [UInt8](textBase64)
                let aes = try createAES(textBytes: textBytes, iv: Array(textBytes.prefix(16)))!
                let decrypted = try aes.decrypt(textBytes[16...])
                result = String(bytes: decrypted, encoding: .utf8) ?? ""
            } else {
                print("decrypto text Base64 decode error")
            }
        } catch CryptoError.runtimeError(let errorMessage){
            print(errorMessage)
        } catch {
            print("decrypto error")
        }
        return result
    }
    
    /**
    切字串，從字串第0個位置一直切到to(字串結束位置)
    - parameter to: 字串結束位置
    */
    func substring(_ range: NSRange) -> String {
        let start = self.index(self.startIndex, offsetBy: range.lowerBound)
        let end = self.index(self.startIndex, offsetBy: range.upperBound)
        let subString = self[start..<end]
        return String(subString)
    }
    
    subscript(_ i: Int) -> String {
      let idx1 = index(startIndex, offsetBy: i)
      let idx2 = index(idx1, offsetBy: 1)
      return String(self[idx1..<idx2])
    }

    subscript (r: Range<Int>) -> String {
      let start = index(startIndex, offsetBy: r.lowerBound)
      let end = index(startIndex, offsetBy: r.upperBound)
      return String(self[start ..< end])
    }

    subscript (r: CountableClosedRange<Int>) -> String {
      let startIndex =  self.index(self.startIndex, offsetBy: r.lowerBound)
      let endIndex = self.index(startIndex, offsetBy: r.upperBound - r.lowerBound)
      return String(self[startIndex...endIndex])
    }
    
    /// 改變字符串中数字的颜色和字體
    ///
    /// - Parameters:
    /// - color: 颜色
    /// - font: 字體
    /// - regx: 正则 默認数字 "\\d+"
    /// - Returns: attributeString
    func strChange(color: UIColor, font: UIFont, regx: String) -> NSMutableAttributedString {
        let attributeString = NSMutableAttributedString(string: self)
        do {
            // 数字正则表达式
            let regexExpression = try NSRegularExpression(pattern: regx, options: NSRegularExpression.Options())
            let result = regexExpression.matches(
                in: self,
                options: NSRegularExpression.MatchingOptions(),
                range: NSMakeRange(0, count)
            )
            for item in result {
                attributeString.setAttributes(
                    [.foregroundColor : color, .font: font],
                    range: item.range
                )
            }
        } catch {
            print("Failed with error: \(error)")
        }
        return attributeString
    }
    
    func replace(target: String, withString: String) -> String {
        return self.replacingOccurrences(of: target, with: withString, options: NSString.CompareOptions.literal, range: nil)
    }
    /// html 轉 string
    /// ex. htmlstring.htmlToAttributedString
    var htmlToAttributedString: NSMutableAttributedString? {
        guard let data = data(using: .utf8) else { return nil }
        do {
            return try NSMutableAttributedString(data: data, options: [.documentType: NSMutableAttributedString.DocumentType.html,.characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch let error as NSError {
            print(error.localizedDescription)
            return  nil
        }
    }

}
