import Alamofire
import CryptoSwift
import CommonCrypto
import UIKit

var developHost = ""

class Api: Codable {
    // MARK: - Variable
    /// 正式
    private lazy var host = "https://store.oinapp.com/"
    private lazy var fileServer = "https://file.oinapp.com/file-server-api"
    /// 測試
//    private lazy var host = "http://test.oinapp.com"
//    private lazy var fileServer = "http://file.downloadapp.club/file-server-api"
    private var url: String!
    private let timeout:Double = 30000
    private var request: Alamofire.Request?
    public lazy var action:ApiAction = ApiAction(rawValue: "none")!
    internal var defaultAction:ApiAction {
        get { preconditionFailure("variable defaultAction must be overridden") } set {}
    }
    private lazy var flagShowLoading:Bool = false
    private var successClosure:((_ obj: AnyObject)->())!
    public var status: Bool? = true
    public var isEncrypt: Bool? = false
    public var message: String? = ""
    private var decoder:Decoder?
    private var fileArray:Array<Data> = []
    private var parameters:Dictionary<String, Any>?
    private lazy var isDecode: Bool = true

    private enum CodingKeys: String, CodingKey {
        case status
        case message
    }
    
    // MARK: - Init
    init(_ viewController: UIViewController?){}

    required init(from decoder: Decoder) throws {
        do {
            self.decoder = decoder
            let container = try decoder.container(keyedBy: CodingKeys.self)
            status = try container.decode(Bool.self, forKey: .status)
            message = try container.decode(String.self, forKey: .message)
        } catch {
            print("\(#file), line: \(#line), \(error)")
        }
    }
    
    // MARK: - func
    func addFile(_ file:Data) -> Api {
        self.fileArray.append(file)
        return self
    }
    
    func setFileArray(_ fileArray:Array<Data>) -> Api {
        self.fileArray = fileArray
        return self
    }
    
    func setParameters(_ parameters:Dictionary<String, Any>) -> Api {
        self.parameters = parameters
        return self
    }
    
    func setShowLoading(_ show: Bool) -> Api {
        self.flagShowLoading = show
        return self
    }
    
    func isDecode(_ isDecode: Bool) -> Api {
        self.isDecode = isDecode
        return self
    }
    
    func setAction(_ action: ApiAction) -> Api {
        self.action = action;
        return self;
    }
    
    private func sent() {
        let parameter = try! JSONEncoder().encode(self)
        
        if self.defaultAction == .None {
            print("please define api action")
            return
        }
        
        if developHost != "" {
            self.host = developHost
        }
        
        let parameters: Dictionary<String,AnyObject> = [
            "action": (self.action != .None ? self.action.rawValue : self.defaultAction.rawValue) as AnyObject,
            "parameters": isEncrypt == true ? String(data: parameter, encoding: .utf8)!.encrypto() as AnyObject : String(data: parameter, encoding: .utf8) as AnyObject
        ]
        print(self.host + "/api" + self.url)
        print("api parameters: \(parameters.description)")
        let headers: HTTPHeaders = [
            "Csrf-Token": "nF3mu/EOHGG7XfViQA+wXBj08T9iLhWuMZlkXm/r",
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Auth-Token": ""
        ]
        
        self.request = AF.request(self.host + "/api" + self.url, method: .post , parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { response in
                switch response.result {
                case .success(let data):
                    do {
                        let json = try JSONSerialization.data(withJSONObject: data, options: [])

                        print("\(self.action == .None ? self.defaultAction.rawValue : self.action.rawValue) response: \(String(data: json, encoding: String.Encoding.utf8) ?? "")");
                        let decode = try JSONDecoder().decode(type(of: self), from: json)
                        if !decode.status! && decode.message == "logout" {}
                        
                        if self.successClosure != nil {
                            if self.isDecode {
                                self.successClosure(decode)
                            } else {
                                
                                self.successClosure(String(data: json, encoding: String.Encoding.utf8) as AnyObject)
                            }
                        }
                    } catch {
                        print("\(#file), line: \(#line), \(error)")
                    }
                    break;
                case .failure(let error):
                    print("api error: \(error)")
                    break;
                }
            }
    }
    func upload(success:@escaping (_ obj:Any)->()) {
        AF.upload(multipartFormData: { multipartFormData in
            self.fileArray.forEach {
                multipartFormData.append($0, withName: "file",fileName: "file.jpeg", mimeType: "image/jpeg")
            }

            for (key, value) in self.parameters! {
                multipartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to: fileServer)
        .uploadProgress { progress in
            self.LoadedView()
            if progress.fractionCompleted == 1{
                self.removeLoadedView()
            }
        }
        .validate(statusCode: 200..<300)
        .responseJSON(completionHandler: { response in
            do {
                let decode = try JSONDecoder().decode(type(of: self), from: response.data!)
                success(decode)
            } catch {
                print("\(#file), line: \(#line), \(error)")
            }
        })
     }
    
    func fetch(success:@escaping (_ obj:AnyObject)->()) {
        self.successClosure = success
        self.url = ""
        self.isEncrypt = false
        self.sent()
    }
    
    func fetchWithEncrypt(success:@escaping (_ obj:AnyObject)->()) {
        self.successClosure = success
        self.url = ""
        self.isEncrypt = true
        self.sent()
    }
    
    func fetchWithAuth(success:@escaping (_ obj:AnyObject)->()) {
        self.successClosure = success
        self.url = "/auth"
        self.isEncrypt = false
        self.sent()
    }
    
    func fetchWithAuthEncrypt(success:@escaping (_ obj:AnyObject)->()) {
        self.successClosure = success
        self.url = "/auth"
        self.isEncrypt = true
        self.sent()
    }
    
    func cancelReq(){
        self.request?.task?.cancel()
        self.request?.cancel()
    }
    
    lazy var subView = UIView()
    func LoadedView(){
        self.subView.backgroundColor = .black
        self.subView.alpha = 0.3
        self.subView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        self.subView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        let currentWindow = UIApplication.shared.windows.first { $0.isKeyWindow }

        currentWindow!.addSubview(self.subView)
        
        let loadingViewWidth = UIScreen.main.bounds.width * 0.15
        let loadingViewHeight = loadingViewWidth
        let loadingView = LoadingView(frame: CGRect(x: UIScreen.main.bounds.width / 2 - loadingViewWidth / 2, y: UIScreen.main.bounds.height / 2 - loadingViewHeight / 2, width: loadingViewWidth, height: loadingViewHeight))
        self.subView.addSubview(loadingView)
        
    }
    
    func removeLoadedView(){
        self.subView.removeFromSuperview()
    }
    
//    @objc func reachabilityChanged(note: Notification) {
//
//        let reachability = note.object as! Reachability
//
//        switch reachability.connection {
//            case .wifi, .cellular:
//                isNetworking = true
//                print("Reachable via WiFi or Cellular")
//            case .none:
//                isNetworking = false
//                print("Network not reachable")
//        }
//    }

}
