//
//  UIStackView.swift
//  oin_store
//
//  Created by dada on 2020/3/5.
//  Copyright © 2020 dada. All rights reserved.
//

import UIKit

extension UIStackView {
    func addArrangedSubviews(_ views: [UIView?]) {
        views.compactMap({ $0 }).forEach { addArrangedSubview($0) }
    }
}
