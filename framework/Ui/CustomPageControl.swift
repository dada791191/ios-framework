//
//  CustomPageControl.swift
//  oin_store
//
//  Created by admin on 2020/3/11.
//  Copyright © 2020 admin. All rights reserved.
//

import UIKit

class CustomPageControl: UIPageControl {

    override func layoutSubviews() {
        super.layoutSubviews()
        
        guard !subviews.isEmpty else { return }
        
        let spacing: CGFloat = 16
        
        let width: CGFloat = 36
        
        let height: CGFloat = 4
        
        var total: CGFloat = 0
        
        for view in subviews {
            view.layer.cornerRadius = 2
            view.frame = CGRect(x: total, y: frame.size.height / 2 - height / 2, width: width, height: height)
            total += width + spacing
        }
        
        total -= spacing
        
        frame.origin.x = frame.origin.x + frame.size.width / 2 - total / 2
        frame.size.width = total
    }

}
