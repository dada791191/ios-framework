//
//  BaseUIViewController.swift
//  p2p_ios
//
//  Created by 顏巨鴻 on 2018/11/21.
//  Copyright © 2018 顏巨鴻. All rights reserved.
//

import UIKit
import WebKit

class BaseUIViewController: UIViewController {
    // MARK: - Variable
    public var apiKey = "a87a5674a70ff9dad49f"
    public var fileUrl = "https://file.oinapp.com/"
    private var keyboardDismissTapGesture: UIGestureRecognizer?
    lazy var isShowAnimation:Bool = false // 是否顯示鍵盤動畫旗標
    lazy var isAddKeyboardEvent:Bool = false // 是否顯示鍵盤動畫旗標
    internal var webView: WKWebView!
    internal lazy var webSubdirectory: String = ""
    lazy var subView = UIView()
    lazy var modalView = UIView()
    enum KeyboardStatus {
        case Show, Hide
    }
    
    // MARK: - override
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - NotificationCenter
    @objc func popView(){
        self.view.endEditing(true)
        self.navigationController!.popViewController(animated: true)
    }
    
    @objc func dismissView(){
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    internal func presentView(storyboard:String, identifier:String, title:String = "", navigationImage:String = "", isNavigationBarHidden:Bool = false) -> UIViewController {
        let vc = UIStoryboard(name: storyboard, bundle: nil).instantiateViewController(withIdentifier: identifier)
        
        if navigationImage != "" {
            vc.addNavigationItem(#selector(dismissView), position: .Left, title: "", img: navigationImage)
        }
        
        if title != "" {
            vc.setupNavigationTitle(title)
        }
        
        let navc = UINavigationController(rootViewController: vc)
        navc.modalPresentationStyle = .fullScreen
        navc.isNavigationBarHidden = isNavigationBarHidden
        
        navc.navigationBar.barTintColor = UIColor.white
        navc.navigationBar.shadowImage = UIImage(color: .clear, size: CGSize(width: 1, height: 1))
        
        self.present(navc, animated: true, completion: nil)
        return vc
    }
    
    internal func pushView(storyboard:String, identifier:String, title:String = "", navigationImage:String = "") -> UIViewController {
        let vc = UIStoryboard(name:storyboard, bundle: nil).instantiateViewController(withIdentifier: identifier)
        
        if navigationImage != "" {
            vc.addNavigationItem(#selector(popView), position: .Left, title: "", img: navigationImage)
        }
        
        if title != "" {
            vc.setupNavigationTitle(title)
        }

        navigationController?.navigationBar.barTintColor = UIColor.white
        navigationController?.navigationBar.shadowImage = UIImage(color: .clear, size: CGSize(width: 1, height: 1))
        
        navigationController?.isNavigationBarHidden = false
        navigationController?.pushViewController(vc, animated: true)
        return vc
    }
    
    func LoadedView(){
        self.subView.subviews.forEach(){$0.removeFromSuperview()}
        self.subView.backgroundColor = .black
        self.subView.alpha = 0.3
        self.subView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        self.subView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        let currentWindow = UIApplication.shared.windows.first { $0.isKeyWindow }

        currentWindow!.addSubview(self.subView)
        
        let loadingViewWidth = UIScreen.main.bounds.width * 0.15
        let loadingViewHeight = loadingViewWidth
        let loadingView = LoadingView(frame: CGRect(x: UIScreen.main.bounds.width / 2 - loadingViewWidth / 2, y: UIScreen.main.bounds.height / 2 - loadingViewHeight / 2, width: loadingViewWidth, height: loadingViewHeight))
        self.subView.addSubview(loadingView)
    }
    
    func removeLoadedView(){
        self.subView.subviews.forEach(){$0.removeFromSuperview()}
        self.subView.removeFromSuperview()
    }
    
    func ShowModal(modalContentView: UIView){
        self.modalView.subviews.forEach(){$0.removeFromSuperview()}
        self.modalView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        self.modalView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        self.modalView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        let gesture:TapGestureRecognizer = TapGestureRecognizer(target: self, action: #selector(removeModal))
        gesture.numberOfTouchesRequired = 1
        
        modalView.isUserInteractionEnabled = true
        modalView.addGestureRecognizer(gesture)
        
        let currentWindow = UIApplication.shared.windows.first { $0.isKeyWindow }
        currentWindow!.addSubview(self.modalView)
        
        self.modalView.addSubview(modalContentView)
    }
    
    @objc func removeModal(){
        self.modalView.subviews.forEach(){$0.removeFromSuperview()}
        self.modalView.removeFromSuperview()
    }
}

