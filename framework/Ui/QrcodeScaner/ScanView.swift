//
//  ScanView.swift
//  like_life
//
//  Created by 顏巨鴻 on 2019/11/19.
//  Copyright © 2019 顏巨鴻. All rights reserved.
//

import Foundation
import UIKit

public class ScanView: UIView {
    private lazy var scanAnimationImage = UIImage()
    private lazy var borderLineWidth:CGFloat = 0.2
    private lazy var cornerColor = UIColor.white
    private lazy var cornerWidth:CGFloat = 2.0
    private lazy var backgroundAlpha:CGFloat = 0.6
    private lazy var scanBorderWidthRadio:CGFloat = 0.6
    private let screenWidth = UIScreen.main.bounds.width
    private let screenHeight = UIScreen.main.bounds.height
    private lazy var scanBorderWidth = scanBorderWidthRadio * screenWidth
    private lazy var scanBorderHeight = scanBorderWidth
    private lazy var scanBorderX = 0.5 * (1 - scanBorderWidthRadio) * screenWidth
    private lazy var scanBorderY = 0.4 * (screenHeight - scanBorderWidth)
    private lazy var contentView = UIView(frame: CGRect(x: scanBorderX, y: scanBorderY, width: scanBorderWidth, height:scanBorderHeight))
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    override public func draw(_ rect: CGRect) {
        super.draw(rect)
        
        drawScan(rect)

        var rect:CGRect?
        let imageView = UIImageView(image: scanAnimationImage.changeColor(cornerColor))
        rect = CGRect(x: 0 , y: -(12 + 20), width: scanBorderWidth , height: 12)
        contentView.backgroundColor = .clear
        contentView.clipsToBounds = true

        addSubview(contentView)

        ScanAnimation.shared.startWith(rect!, contentView, imageView: imageView)

    }
    
    private func setup() {
        backgroundColor = .clear
        borderColor = UIColor.white
    }
    
    func drawScan(_ rect: CGRect) {
        if UIScreen.main.bounds.size.height != 568 {
            self.scanBorderWidthRadio = 0.7
        }
        
        if UIScreen.main.bounds.size.height == 667 { // i8
            self.scanBorderY = 0.37 * (screenHeight - scanBorderWidth)
        } else if UIScreen.main.bounds.size.height == 736 { // i8 plus
            self.scanBorderY = 0.35 * (screenHeight - scanBorderWidth)
        } else if UIDevice().name == "iPhone 11 Pro Max" || UIDevice().name == "iPhone 11" || UIScreen.main.bounds.size.height == 812 { // i11 pro max
            self.scanBorderY = 0.3 * (screenHeight - scanBorderWidth)
        }
        
        UIColor.black.withAlphaComponent(backgroundAlpha).setFill()
        
        UIRectFill(rect)
        
        let context = UIGraphicsGetCurrentContext()
        
        context?.setBlendMode(.destinationOut)
        
        let bezierPath = UIBezierPath(rect: CGRect(x: scanBorderX + 0.5 * borderLineWidth, y: scanBorderY + 0.5 * borderLineWidth, width: scanBorderWidth - borderLineWidth, height: scanBorderHeight - borderLineWidth))
        
        bezierPath.fill()
        
        context?.setBlendMode(.normal)
        let borderPath = UIBezierPath(rect: CGRect(x: scanBorderX, y: scanBorderY, width: scanBorderWidth, height: scanBorderHeight))
        
        borderPath.lineCapStyle = .butt
        borderPath.lineWidth = borderLineWidth
        borderColor!.set()
        borderPath.stroke()
        
        let cornerLenght:CGFloat = 20
        
        // 左上
        let leftTopPath = UIBezierPath()
        leftTopPath.lineWidth = cornerWidth
        cornerColor.set()
        leftTopPath.move(to: CGPoint(x: scanBorderX, y: scanBorderY + cornerLenght))
        leftTopPath.addLine(to: CGPoint(x: scanBorderX, y: scanBorderY))
        leftTopPath.addLine(to: CGPoint(x: scanBorderX + cornerLenght, y: scanBorderY))
        leftTopPath.stroke()
        
        // 左下
        let leftBottomPath = UIBezierPath()
        leftBottomPath.lineWidth = cornerWidth
        cornerColor.set()
        leftBottomPath.move(to: CGPoint(x: scanBorderX + cornerLenght, y: scanBorderY + scanBorderHeight))
        leftBottomPath.addLine(to: CGPoint(x: scanBorderX, y: scanBorderY + scanBorderHeight))
        leftBottomPath.addLine(to: CGPoint(x: scanBorderX, y: scanBorderY + scanBorderHeight - cornerLenght))
        leftBottomPath.stroke()
        
        // 右上
        let rightTopPath = UIBezierPath()
        rightTopPath.lineWidth = cornerWidth
        cornerColor.set()
        rightTopPath.move(to: CGPoint(x: scanBorderX + scanBorderWidth - cornerLenght, y: scanBorderY))
        rightTopPath.addLine(to: CGPoint(x: scanBorderX + scanBorderWidth, y: scanBorderY))
        rightTopPath.addLine(to: CGPoint(x: scanBorderX + scanBorderWidth, y: scanBorderY + cornerLenght))
        rightTopPath.stroke()
        
        // 右下
        let rightBottomPath = UIBezierPath()
        rightBottomPath.lineWidth = cornerWidth
        cornerColor.set()
        rightBottomPath.move(to: CGPoint(x: scanBorderX + scanBorderWidth, y: scanBorderY + scanBorderHeight - cornerLenght))
        rightBottomPath.addLine(to: CGPoint(x: scanBorderX + scanBorderWidth, y: scanBorderY + scanBorderHeight))
        rightBottomPath.addLine(to: CGPoint(x: scanBorderX + scanBorderWidth - cornerLenght, y: scanBorderY + scanBorderHeight))
        rightBottomPath.stroke()
    }
}
