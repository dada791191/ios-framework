//
//  LocationManager.swift
//  like_life
//
//  Created by 顏巨鴻 on 2019/11/5.
//  Copyright © 2019 顏巨鴻. All rights reserved.
//

import Foundation
import CoreLocation

class LocationManager: NSObject {
    private lazy var locationManager: CLLocationManager? = nil
    lazy var lat:Double = 0.0
    lazy var lng:Double = 0.0
    private var closure:((_ lat:Double, _ lng:Double)->())!
    
    override init() {
        super.init()
        self.locationManager = CLLocationManager()
        self.locationManager!.delegate = self
        self.locationManager!.desiredAccuracy = kCLLocationAccuracyBest
    }
    
    func start(_ closure:@escaping (_ lat:Double, _ lng:Double)->()) -> LocationManager {
        self.closure = closure
        if (CLLocationManager.locationServicesEnabled()) {
            self.locationManager!.requestWhenInUseAuthorization()
            self.locationManager!.startUpdatingLocation()
        }
        return self
    }
    
    func stop () {
        self.locationManager!.stopUpdatingLocation()
    }
}

// MARK: - CLLocationManagerDelegate
extension LocationManager: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last! as CLLocation
        self.lat = location.coordinate.latitude
        self.lng = location.coordinate.longitude
        self.closure(lat, lng)
    }
}
