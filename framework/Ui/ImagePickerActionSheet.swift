//
//  ActionSheet.swift
//  like_life
//
//  Created by 顏巨鴻 on 2019/11/27.
//  Copyright © 2019 顏巨鴻. All rights reserved.
//

import Foundation
import UIKit

class ImagePickerActionSheet: NSObject {
    var imageData:Data?
    var base64:String?
    var viewController:UIViewController?
    
    func show(_ viewController:UIViewController) {
        self.viewController = viewController
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "取消", style: .cancel, handler: nil))
        actionSheet.addAction(UIAlertAction(title: "拍照", style: .default, handler: openCamera))
        actionSheet.addAction(UIAlertAction(title: "相簿", style: .default, handler: openPhotoLibrary))
        self.viewController!.present(actionSheet, animated: true)
    }
    
    private func openCamera(_: UIAlertAction){
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera){
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = (viewController as! UIImagePickerControllerDelegate & UINavigationControllerDelegate)
            imagePicker.sourceType = UIImagePickerController.SourceType.camera;
            imagePicker.allowsEditing = true
            imagePicker.showsCameraControls = true
            imagePicker.cameraFlashMode = UIImagePickerController.CameraFlashMode.off
            imagePicker.cameraViewTransform = CGAffineTransform(scaleX: 1.5, y: 1.5);
            imagePicker.modalPresentationStyle = .fullScreen
            self.viewController!.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    private func openPhotoLibrary(_: UIAlertAction){
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = (viewController as! UIImagePickerControllerDelegate & UINavigationControllerDelegate)
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary;
            imagePicker.allowsEditing = true
            imagePicker.modalPresentationStyle = .fullScreen
            self.viewController!.present(imagePicker, animated: true, completion: nil)
        }
    }
}
