//
//  UIButton.swift
//  p2p_ios
//
//  Created by 顏巨鴻 on 2018/11/22.
//  Copyright © 2018 顏巨鴻. All rights reserved.
//

import UIKit
@IBDesignable extension UIButton {
    @IBInspectable var btnCornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }
    
    func roundCorners(corners: CACornerMask, radius: CGFloat) {

        self.layer.cornerRadius = radius
        self.clipsToBounds = true
        
        self.layer.maskedCorners = corners

    }
}
