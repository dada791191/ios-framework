//
//  TapGestureRecognizer.swift
//  like_life
//
//  Created by 顏巨鴻 on 2019/11/12.
//  Copyright © 2019 顏巨鴻. All rights reserved.
//

import Foundation
import UIKit

class TapGestureRecognizer: UITapGestureRecognizer {
    var action: String = ""
    var index: Int = 0
}
