//
//  ApiAction.swift
//  like_life
//
//  Created by 顏巨鴻 on 2019/10/17.
//  Copyright © 2019 顏巨鴻. All rights reserved.
//

import Foundation

public enum ApiAction: String {
    case None = ""
    case GetApiToken = "GetApiToken"
}
