//
//  Sqlite.swift
//  p2p_ios
//
//  Created by 顏巨鴻 on 2018/12/24.
//  Copyright © 2018 顏巨鴻. All rights reserved.
//

import Foundation
import UIKit
import SQLite3

func setupDatabaseTable() {
    let db = Sqlite()
    
    let sqlGoodsType = "create table if not exists goods_types (id integer, type_name text, super_type_id integer, level integer);"
    db.execute(sql: sqlGoodsType)
    
}

class Sqlite{
    var _dbPath:String?
    var _db: OpaquePointer?
    
    init() {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        _dbPath = urls[urls.count-1].absoluteString + "sqlite.db"
    }
    
    func query(sql:String, result:@escaping (_ statement:OpaquePointer)->()){
        dbOpen()
        var statement:OpaquePointer?
        sqlite3_prepare_v2(_db, sql, -1, &statement, nil)
        while sqlite3_step(statement) == SQLITE_ROW {
            result(statement!)
        }
        sqlite3_finalize(statement)
        sqlite3_close(_db)
    }
    
    func execute(sql:String){
        dbOpen()
        if sqlite3_exec(_db, sql, nil, nil, nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(_db)!)
            print("error create table: \(errmsg)")
        }
        sqlite3_close(_db)
    }
    
    private func dbOpen(){
        if sqlite3_open(_dbPath, &_db) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(_db)!)
            print("error open database: \(errmsg)")
        }
    }
}
