//
//  Token.swift
//  p2p_ios
//
//  Created by 顏巨鴻 on 2018/11/13.
//  Copyright © 2018 顏巨鴻. All rights reserved.
//

import UIKit
import Foundation

/// struct
struct TokenModel : Decodable {
    var compositionType: Int?
    private enum CodingKeys: String, CodingKey {
        case compositionType
    }
}

final class Token: Api {
    /// setting default action
    override internal var defaultAction:ApiAction {
        get { return .GetApiToken } set {}
    }
    /// setting parameter
    private enum CodingKeys: String, CodingKey {
        case data
        case appName
        case token
        case os
    }
    private var appName = { () -> String? in
        let bundleID = Bundle.main.bundleIdentifier
        return bundleID
    }()
    private var token: String?
    private var os: String = "ios"
    public var data: Any?
    static  let sharedInstance :Token = Token(nil)
    
    // MARK: - Init
    override init(_ viewController: UIViewController?) { super.init(nil) }
    
    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Array<TokenModel>.self, forKey: .data) {
            self.data = value
        } else if let value = try? container.decode(TokenModel.self, forKey: .data) {
            self.data = value
        } else if let value = try? container.decode(String.self, forKey: .data){
            self.data = value
        }
    }
    
    // MARK: - func
    override func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(appName, forKey: .appName)
        try container.encode(token, forKey: .token)
        try container.encode(os, forKey: .os)
        try super.encode(to: encoder)
    }
    
    func setDeviceToken(_ deviceToken:String) -> Token {
        self.token = deviceToken
        return self
    }
}
